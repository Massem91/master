#include "widget.h"
#include "ui_widget.h"
#include <qwt_legend.h>
#include <qwt.h>

Widget::Widget(QWidget *parent) : QwtPlot(parent),
     ui(new Ui::Widget)
{
     ui->setupUi(this);
     this->setupPlot();
}

Widget::~Widget()
{
     delete ui;
}

void Widget::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
void Widget::setupPlot() {

    this->setTitle("Qwt Tutorial");
    this->setCanvasBackground(QColor(Qt::white));

    this->setAutoReplot(false);
    //this->setMargin(5);
    QwtLegend *legend = new QwtLegend;
       legend->setFrameStyle(QFrame::Box|QFrame::Sunken);
       this->insertLegend(legend, QwtPlot::BottomLegend);

       // axis
       this->setAxisTitle(QwtPlot::xBottom, "X Axis");
       this->setAxisTitle(QwtPlot::yLeft, "Y Axis");
}
