#include "widget.h"
#include<QCoreApplication>
#include <QApplication>
#include <QtGui>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include<qwt_point_data.h>
#include<qfile.h>
#include<QVBoxLayout>
#include <qtextstream.h>
#include <qstringlist.h>
#include<qmessagebox.h>
#include<iostream>
#include<cstdlib>
#include<fixationfilter.h>
#include <qwt_plot_magnifier.h>
#include<qwt_plot_zoomer.h>
#include<qwt_text_label.h>
#include<qwt_plot_textlabel.h>
#include<realtime.h>
#include<qwt_plot_marker.h>
#include<qwt_symbol.h>
RealTime::RealTime()
{

    xgaze= new double [2000];
    ygaze= new double [2000];
    time= new double  [2000];
    counter=0;

    // Creating two plots for the x-gaze and the y-gaze and then they will be added to the layout in the slot

    myPlotx = new QwtPlot(NULL);
    myPlotx->setWindowTitle("2-D Plotting");
    myPlotx->setAxisTitle(QwtPlot::xBottom,"Time");
    myPlotx->setAxisTitle(QwtPlot::yLeft,"X-Gaze");
    myPloty = new QwtPlot(NULL);
    myPloty->setWindowTitle("2-D Plotting");
    myPloty->setAxisTitle(QwtPlot::xBottom,"Time");
    myPloty->setAxisTitle(QwtPlot::yLeft,"Y-Gaze");
    myPlotx->setAutoReplot(true);
    myPloty->setAutoReplot(true);
    curve1 = new QwtPlotCurve("Curve 1");
    curve2 = new QwtPlotCurve("Curve 2");
    window = new QWidget;
    layout = new QVBoxLayout;



}

// The OnlinePlot function takes the x,y gaze pixels and their corresponding time then plots every new point passed from the signal
// with the previous ones

void RealTime::OnlinePlot(double x,double y,double t)

{
    xgaze[counter]=x;
    ygaze[counter]=y;
    time[counter]=time;
    curve1->setData(new QwtCPointerData(&time[0],&xgaze[0],(size_t)counter));
    curve1->attach( myPlotx );
    curve2->setData(new QwtCPointerData(&time[0],&ygaze[0],(size_t)counter));
    curve2->attach( myPloty );
    layout->addWidget(myPlotx);
    layout->addWidget(myPloty);
    window->setLayout(layout);
    window->show();
    counter++;
      }


        /*
        QwtPlotMarker* m = new QwtPlotMarker();
    QwtSymbol *Symboly = new QwtSymbol (QwtSymbol::Diamond, Qt::red, Qt::NoPen, QSize( 10, 10 ) );
            m->setSymbol( Symboly);
            m->setValue( QPointF( 15, 22 ) );
            */




