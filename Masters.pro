#-------------------------------------------------
#
# Project created by QtCreator 2016-04-03T10:57:12
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Masters
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    fixationfilter.cpp \
    realtime.cpp

HEADERS  += widget.h \
    fixationfilter.h \
    realtime.h

CONFIG   +=qwt
CONFIG += console
FORMS    += widget.ui

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../qwt-6.1.2/lib/ -lqwt
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../qwt-6.1.2/lib/ -lqwtd
else:unix: LIBS += -L$$PWD/../../../../qwt-6.1.2/lib/ -lqwt

INCLUDEPATH += $$PWD/../../../../qwt-6.1.2/include
DEPENDPATH += $$PWD/../../../../qwt-6.1.2/include

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../Qt/Qt5.5.1/5.5/msvc2010/lib/ -lqtmain
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../Qt/Qt5.5.1/5.5/msvc2010/lib/ -lqtmaind
else:unix: LIBS += -L$$PWD/../../../../Qt/Qt5.5.1/5.5/msvc2010/lib/ -lqtmain

INCLUDEPATH += $$PWD/../../../../Qt/Qt5.5.1/5.5/msvc2010/include
DEPENDPATH += $$PWD/../../../../Qt/Qt5.5.1/5.5/msvc2010/include
