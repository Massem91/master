#include "widget.h"
#include<QCoreApplication>
#include <QApplication>
#include <QtGui>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include<qwt_point_data.h>
#include<qfile.h>
#include<QVBoxLayout>
#include <qtextstream.h>
#include <qstringlist.h>
#include<qmessagebox.h>
#include<iostream>
#include<cstdlib>
#include<fixationfilter.h>
#include <qwt_plot_magnifier.h>
#include<qwt_plot_zoomer.h>
#include<qwt_text_label.h>
#include<qwt_plot_textlabel.h>
#include<realtime.h>
#include <cmath>
using namespace std;
 double TP,TN,FP,FN = 0;
 double Count=0;
double Accuracy;
void ConfusionMatrix(int Known_Value,int Measured_Value){

    Count++;
     if (Known_Value == 1){
        if(Measured_Value == 1)
            TP++;
            else
            FN++;

    }
    else{
        if (Known_Value == 0){
            if(Measured_Value == 0)
                TN++;
                else
                FP++;

        }
}

}

int main(int argc, char **argv)
{
  QApplication app(argc, argv);

    QwtPlot *myPlot = new QwtPlot(NULL);
    QwtPlot *myPlot1 = new QwtPlot(NULL);
    QwtPlot *myPlot2 = new QwtPlot(NULL);

    // add curves
    QwtPlotCurve *curve1 = new QwtPlotCurve("Curve 1");
    QwtPlotCurve *curve2 = new QwtPlotCurve("Curve 2");
    QwtPlotCurve *curve3 = new QwtPlotCurve("Curve 3");



    // first plot of the x-gaze samples
    //myPlot->setFixedWidth(500);
    myPlot->setWindowTitle("2-D Plotting");
    myPlot->setAxisTitle(QwtPlot::xBottom,"Time");
    myPlot->setAxisTitle(QwtPlot::yLeft,"X-Gaze");
    myPlot->setAutoReplot(true);

  //Plot Zooming

    QwtPlotZoomer* zoomer = new QwtPlotZoomer(myPlot->canvas());
    zoomer->setKeyPattern( QwtEventPattern::KeyLeft, Qt::LeftButton);

    QwtPlotZoomer* zoomer1 = new QwtPlotZoomer(myPlot2->canvas());
    zoomer1->setKeyPattern( QwtEventPattern::KeyRedo, Qt::Key_I, Qt::ShiftModifier );
    zoomer1->setKeyPattern( QwtEventPattern::KeyUndo, Qt::Key_O, Qt::ShiftModifier );
    zoomer1->setKeyPattern( QwtEventPattern::KeyHome, Qt::Key_Home );

    // Second plot of the y-gaze samples
    //myPlot1->setFixedWidth(500);
    myPlot1->setWindowTitle("2-D Plotting");
    myPlot1->setAxisTitle(QwtPlot::xBottom,"Time");
    myPlot1->setAxisTitle(QwtPlot::yLeft,"Y-Gaze");

    //third plot for detection of fixation and saccade
    myPlot2->setWindowTitle("2-D Plotting");
    myPlot2->setAxisTitle(QwtPlot::xBottom,"Time");
    myPlot2->setAxisTitle(QwtPlot::yLeft,"Fix/sacc Detection");


//   /* // Read GazePoints
    QFile file("C:/Users/mohamed/Desktop/MasterThesis/ExperimentData/Shiva/POR.txt");
    if(!file.open(QIODevice::ReadOnly)) {
        QMessageBox::information(0, "error", file.errorString());
    }

    QTextStream in(&file);
    int counter=0;
    QStringList time_Gaze;
    QStringList x_Gaze;
    QStringList y_Gaze;
    QStringList Angular_Velocity;

    while(!in.atEnd()) {
       QString line = in.readAll();
         counter++;
        QStringList fields = line.split(";");
       for (int i=0;i<fields.size()-2;i=i+3){

        time_Gaze.append(fields[i]);
        x_Gaze.append(fields[i+1]);
        y_Gaze.append(fields[i+2]);


      /*X_Gaze=x_Gaze.toDouble();
      Time_Gaze=time_Gaze.toDouble();
      Y_Gaze=y_Gaze.toDouble();
      */

}



        qDebug()<< counter;
    }

    file.close();
/*
    //testing filtered data from matlab

    QFile fileX("C:/Users/mohamed/Desktop/MasterThesis/Code/FilteredGazeX.txt");
    if(!fileX.open(QIODevice::ReadOnly)) {
        QMessageBox::information(0, "error", fileX.errorString());
    }

    QTextStream inx(&fileX);

    while(!inx.atEnd()) {
       QString line = inx.readLine();
         counter++;
        QStringList fields = line.split(";");
       for (int i=0;i<fields.size();i++){
        x_Gaze.append(fields[i]);

}

    }

    fileX.close();

    QFile fileY("C:/Users/mohamed/Desktop/MasterThesis/Code/FilteredGazeY.txt");
    if(!fileY.open(QIODevice::ReadOnly)) {
        QMessageBox::information(0, "error", fileY.errorString());
    }

    QTextStream iny(&fileY);

    while(!iny.atEnd()) {
       QString line = iny.readAll();
         counter++;
        QStringList fields = line.split(";");
       for (int i=0;i<fields.size();i++){
        y_Gaze.append(fields[i]);

}

    }

    fileY.close();

    QFile fileW("C:/Users/mohamed/Desktop/MasterThesis/Code/AngularVelocity.txt");
    if(!fileW.open(QIODevice::ReadOnly)) {
        QMessageBox::information(0, "error", fileW.errorString());
    }

    QTextStream inw(&fileW);

    while(!inw.atEnd()) {
       QString line = inw.readAll();
         counter++;
        QStringList fields = line.split(";");
       for (int i=0;i<fields.size();i++){
        Angular_Velocity.append(fields[i]);

}

    }

    fileW.close();
*/


    double w[1000];

double *x= new double [time_Gaze.size()];
double *t= new double [time_Gaze.size()];
double *y= new double [time_Gaze.size()];

    for (int i=0;i<time_Gaze.size()-1;i++){

        x[i]=x_Gaze[i+1].toDouble();


        t[i]=time_Gaze[i+1].toDouble();
     //   if (t[i]<0)

        y[i]=y_Gaze[i+1].toDouble();
       //w[i]=Angular_Velocity[i+1].toDouble();
       // qDebug()<< i;


    }



    //Determining the thresholdvalue to detect the fixation and saccade
    //fixationfilter takes the threshold value and the minimum fixation samples

FixationFilter fix_sacc_detection(5,5);
double * x_fix = new double [time_Gaze.size()] ;
double * y_fix = new double [time_Gaze.size()] ;
double * fix_det = new double [time_Gaze.size()] ;
double * Expected = new double [time_Gaze.size()] ;
bool Gaze_found=true;

for (int i=0;i<time_Gaze.size()-1;i++){

   fix_det[i]= (double) fix_sacc_detection.detectFixation(Gaze_found,x[i],y[i]);

   Expected[i]=1;


   if (fix_det[i]==2) fix_det[i]=1;
            x_fix[i]=fix_sacc_detection.getFixation_x();
            y_fix[i]=fix_sacc_detection.getFixation_y();


}


//Checking the ConfusionMatrix
for (int i=0;i<time_Gaze.size()-1;i++){
ConfusionMatrix(Expected[i],fix_det[i]);
}
Accuracy = (TP+TN)/Count;
qDebug() << Accuracy;

    curve1->setData(new QwtCPointerData(&t[200],&x[200],(size_t)1000));
    curve2->setData(new QwtCPointerData(&t[200],&y[200],(size_t)1000));
    curve3->setData(new QwtCPointerData(&t[200],&fix_det[200],(size_t)1000));
    //curve1->setData(new QwtCPointerData(&t[0],&w[0],(size_t)900));


    curve1->attach(myPlot);
    curve2->attach(myPlot1);
    curve3->attach(myPlot2);

    //some random tests to rey the realtime plotting



  //  RealTime R;
    // finally, refresh the plot
   QWidget *window = new QWidget;
    QVBoxLayout *layout = new QVBoxLayout;
          layout->addWidget(myPlot);
          layout->addWidget(myPlot1);
          layout->addWidget(myPlot2);
   //   myPlot->replot();
   //myPlot->show();
        window->setLayout(layout);
              window->show();

return app.exec();
}


