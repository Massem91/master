#ifndef REALTIME_H
#define REALTIME_H

#include<QTimer.h>
#include <qwt_plot.h>
#include<qwt_plot_curve.h>
#include<QVBoxLayout>


class RealTime : public QObject
{
    Q_OBJECT
public:

     RealTime();

     int counter;
     QwtPlot *myPlotx;
     QwtPlot *myPloty;
     QwtPlotCurve *curve1;
     QwtPlotCurve *curve2;
     QWidget *window;
     QVBoxLayout *layout;
     double *xgaze;
     double *ygaze;
     double *time;


public slots:
    void OnlinePlot(double x, double y, double t);
};

#endif // REALTIME_H

